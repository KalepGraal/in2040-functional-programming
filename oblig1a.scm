#| Oppgave 1
a) evaluerer til 30
   (* (+ 4 2) 5)
b) syntaktisk feil, da parantesene i (5) gjør at det forventes en prosedyre
   (* (+ 4 2) (5))
c) syntaktisk feil da det brukes infiks og ikke prefiks for operatoren
   (* (4 + 2) 5)
d) definerer bar til 22, kaller på bar som er 22.
   (define bar (/ 44 2))
   bar
e) bar er 22, deretter har vi: 22 - 11 = 11
   (- bar 11)
f) Her får vi 12, ettersom 22*3*4*1/22 => 3*4*1 => 12
   (/ (* bar 3 4 1) bar)

Oppgave 2
a)
 #1 evaluerer til "paff!", ettersom dette er det første elementet som ikke er #f
 #2 evaluerer til #f, ettersom (= 1 2) evaluerer til #f.
 #3 evaluerer til "poff!" ettersom (positive? 42) ikke evaluerer til #f

Disse eksemplene viser oss at de er special forms ettersom de syntakiske feilene
aldri blir evaluert. Vanligvis vil Scheme evaluere underuttrykk først, men dette
ville ført til feilmeldinger hvis det hadde blitt gjort her.

b)|#

;;; sign with if
(define (sign x)
  (if (< x 0)
      -1
      (if(> x 0)
         1
         0)))

;;; sign with cond
(define (sign x)
  (cond ((< x 0) -1)
        ((> x 0) 1)
        (else 0)))

;;; c)

(define (sign x)
  (or
   (and (< x 0) -1)
   (and (> x 0) 1)
   0))

;;; Oppgave 3
;;; a)
(define (add1 x) (+ x 1))
(define (sub1 x) (- x 1))
;;; b)
(define (plus x y)
  (if (zero? y) x
      (plus (add1 x) (sub1 y))))
;;; c)
;;; i oppgave b definerte jeg en prosedyre som fører til en iterativ prosess.
;;; Dette sees ettersom minnebruken vil være konstant, og vi benytter tail-call
;;; elimination. x og y fungerer her som akkumulatorvariabler.

;;; Denne prosedyren vil føre til en rekursiv prosess, da vi ikke har noen
;;; akkumulerende verdi, noe som fører til at minnebruken ikke vil være konstant
(define (plus x y)
  (if (zero? y) x
      (add1 (plus x (sub1 y)))))

;;; d)
;;; Her kan vi fjerne to argumenter fra hjelpe-prosedyren, ettersom b og n
;;; allerede er definert i power-close-to
(define (power-close-to b n)
  (define (power-iter e)
    (if (> (expt b e) n)
        e
        (power-iter (+ 1 e))))
  (power-iter 1))

;;; e)
#|
Kan ikke se noen noen måte å forenkle fib-iter hvis vi skriver om til
blokkstruktur. Ettersom ingen av variablene som trengs i fib-iter 
oppstår i fib, kan vi heller ikke fjerne noen fib-iters variabler.
|#