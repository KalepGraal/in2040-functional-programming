;;
;; 1 a
;;
(define (make-counter)
  (let ((count 0))
    (lambda ()
      (set! count (+ count 1))
      count)))

;;
;; 1 b
;;

;; see attached: Oblig2b-1b.png

;;
;; 2 a
;;

(define (make-stack stack)
  (define (push! args)
    (if (not (null? args))
        (begin
          (set! stack (cons (car args) stack))
          (push! (cdr args)))))
  
  (define (pop!)
    (if (not(null? stack))
        (set! stack (cdr stack))))
  
  (lambda (msg . args)
    (cond ((equal? msg 'stack)
           stack)
          ((equal? msg 'pop!)
           (pop!))
          ((equal? msg 'push!)
           (if (null? args)
               push!
               (push! args)))
          (else "unrecognized"))))

(define s1 (make-stack (list 'foo 'bar)))
(define s2 (make-stack '()))
(s1 'pop!)
(s1 'stack); → (bar)
(s2 'pop!) ;; popper en tom stack
(s2 'push! 1 2 3 4)
(s2 'stack); → (4 3 2 1)
(s1 'push! 'bah)
(s1 'push! 'zap 'zip 'baz)
(s1 'stack); → (baz zip zap bah bar)

;;
;; 2 b
;;

(define (pop! s)
  (s 'pop!))

(define (stack s)
  (s 'stack))

(define (push! s . args)
  (apply (s 'push!) (list args)))

(pop! s1)
(stack s1); → (zip zap bah bar)
(push! s1 'foo 'faa)
(stack s1); → (faa foo zip zap bah bar)


;;
;; 3 a
;;

(define bar (list 'a 'b 'c 'd 'e))
(set-cdr! (cdddr bar) (cdr bar))

;; see attached: Oblig2b-3a.png

;;
;; 3 b
;;

(define bah (list 'bring 'a 'towel))
(set-car! bah (cdr bah))
(set-car! (car bah) 42)

;; see attached: Oblig2b-3b.png
;; 
;; Ettersom (car bah) peker til den samme boksen som (cdr bah),
;; vil det si at når vi endrer car verdien til den boksen, vil den
;; også endres begge steder i listen, siden det er samme element
;; referert til to ganger.
;;

;;
;; 3 c
;;

(define (cycle? x)
  (define (cycle-iter i traversed)
    (if (null? (cdr i))
        #f
        (if (member (cdr i) traversed)
            #t
            (cycle-iter (cdr i) (cons i traversed)))))
  (cycle-iter x '()))
 
(cycle? '(hey ho)); → #f
(cycle? '(la la la)); → #f
(cycle? bah); → #f
(cycle? bar); → #t

;;
;; 3 d
;;

;; Antar bar ikke er en liste fordi den ikke inneholder et par
;; hvor cdr gir den tomme listen.
;; bah er en liste fordi den har et par der cdr gir den tomme listen.

;;
;; 3 e
;;

(define (make-ring input)
  (define (create-loop input)
    (define (find-last i)
      (if (null? (cdr i))
          i
          (find-last (cdr i))))
    (set-cdr! (find-last input) input))
  (begin
    ;; Note: using append to make copy of input list
    (define ring (append input '()))
    (create-loop ring)

    (define (find-left-to start)
      ;; finds item to the left of the top in a ring
      (define (iter i)
        (if (equal? (cdr i) start)
            i
            (iter (cdr i))))
      (iter ring))
      
    (let ((top ring)
          (last (find-left-to ring)))
      
      (define (right!)
        (set! top last)
        (set! last (find-left-to top))
        (car top))
      
      (define (left!)
        (set! last top)
        (set! top (cdr top))
        (car top))
      
      (define (delete!)
        (set-cdr! last (cdr top))
        (set! top (cdr top))
        (car top))
      
      (define (insert! x)
        (set-cdr! last (cons x top))
        (set! top (cdr last))
        (car top))

      ;; message passing
      (lambda (msg . arg)
        (cond ((equal? msg 'top)
               (car top))
              ((equal? msg 'right!)
               (right!))
              ((equal? msg 'left!)
               (left!))
              ((equal? msg 'insert!)
               (insert! (car arg)))
              ((equal? msg 'delete!)
               (delete!))
              (else "unrecognized"))))))

(define (top ring)
  (ring 'top))

(define (right-rotate! ring)
  (ring 'right!))

(define (left-rotate! ring)
  (ring 'left!))

(define (insert! ring x)
  (ring 'insert! x))

(define (delete! ring)
  (ring 'delete!))

(define r1 (make-ring '(1 2 3 4)))
(define r2 (make-ring '(a b c d)))

(top r1); → 1
(top r2); → a
(right-rotate! r1); → 4
(left-rotate! r1); → 1
(left-rotate! r1); → 2
(delete! r1); → 3
(left-rotate! r1); → 4
(left-rotate! r1); → 1
(left-rotate! r1); → 3
(insert! r2 'x); → x
(right-rotate! r2); → d
(left-rotate! r2); → x
(left-rotate! r2); → a
(top r1); → 3

;;
;; 3 f
;;

;; kompleksistet:
;; left-rotate: O(1)
;; right-rotate: O(n)
;; insert: O(1)
;; delete: O(1)

;; alle funksjonene kjører i konstant-tid (O(1)), ved unntak av right-rotate.
;; right-rotate vil måtte gå igjennom hele ringen (for å finne neste element
;; til venstre for topp), og dermed vil den ha lineær kompleksitet (O(n)).

