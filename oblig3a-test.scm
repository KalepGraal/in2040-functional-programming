;; Just a bunch of code to test-run oblig3a

(load "oblig3a.scm")

(set! fib (mem 'memoize fib))
(fib 3)
;computing fib of 3
;computing fib of 2
;computing fib of 1
;computing fib of 0
;→ 2
(fib 3)
;→ 2
(fib 2)
;→ 1
(fib 4)
;computing fib of 4
;→ 3



(set! fib (mem 'unmemoize fib))
(fib 3)




(set! test-proc (mem 'memoize test-proc))
(test-proc)
;computing test-proc of ()
;→ 0
(test-proc)
;→ 0
(test-proc 40 41 42 43 44)
;computing test-proc of (40 41 42 43 44)
;computing test-proc of (41 42 43 44)
;computing test-proc of (42 43 44)
;computing test-proc of (43 44)
;computing test-proc of (44)
;→ 10
(test-proc 40 41 42 43 44)
;→ 10
(test-proc 42 43 44)
;→ 5


(greet)
;good day friend
(greet 'time "evening")
;good evening friend
(greet 'title "sir" 'time "morning")
;good morning sir
(greet 'time "afternoon" 'title "dear")
;good afternoon dear



(list-to-stream '(1 2 3 4 5))
;→ (1 . #<promise>)
(stream-to-list (stream-interval 10 20))
;→ (10 11 12 13 14 15 16 17 18 19 20)
(show-stream nats 15)
; 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 ...
(stream-to-list nats 10)
;→ (1 2 3 4 5 6 7 8 9 10)



(newline)
(newline)
(newline)

(define x (stream-interval 0 10))
(define y (integers-starting-from 0))
(define z (integers-starting-from 0))

x
y
z

(stream-map * x y z)
(show-stream (stream-map + x y z))

(define five (stream-map (lambda (x) (modulo x 5)) x))


(newline)



(display "\n\n\n")

(remove-duplicates (list 1 2 3 1 1 1 2 3))
y
(newline)

(remove-duplicates five)
(show-stream five)
(show-stream (remove-duplicates five))

(show-stream (remove-duplicates y))

(display "\n\n\n")

(display "define\n")
(define x
  (stream-map show
              (stream-interval 0 10)))

(display "? (stream-ref x 5)\n") 
(stream-ref x 5)
(display "? (stream-ref x 7)\n")
(stream-ref x 7)

(display "\n\n\n")

(mul-streams y z)
(show-stream (mul-streams y y y))
