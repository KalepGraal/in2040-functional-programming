(load "evaluator.scm")

;;
;; 1 a
;;
;; cond og else brukes her både som lokale variabler i foo, og som predikater.
;; dette fungerer på grunn av rekkefølgen ting evalueres i.
;; Ettersom cond er en special form, vil denne oppdages når meta-REPL'et prøver
;; å evaluere uttrykket, og konverteres til å kun bruke if.
;;

;;
;; (foo 2 square)
;; -> 0
;; Her evalueres det til 0, fordi variabelen cond evalures til 2
;; som igjen er lik 2, altså passerer (= cond 2).
;;
;; (foo 4 square)
;; -> 16
;; Her blir det 16, ettersom variablen cond (4) ikke er likt 2,
;; og variablen else blir kalt som prosedyre på variabelen cond,
;; noe som gir oss (square 4) som igjen evalueres til 16.
;; Verdt å merke seg at else blir tolket som en variabel fordi
;; det sjekkes for dette i mc-eval før det sjekkes om det er en
;; prosedyre.
;;
;; (cond ((= cond 2) 0)
;;     (else (else 4)))
;; -> 2
;; Her får vi 2, da (= cond 2) blir til (= 3 2), noe som feiler
;; og vi ender da opp med (else 4), og ettersom det ikke finnes
;; en else variabel, vil denne evalures til en prosedyre, og vi
;; sitter igjen med: (else 4) => (/ 4 2) => 2.
;;

;;
;; 2 a
;;
;; Endringene ligger markert som 2 a i evaluator.scm
;;

;;
;; 2 b
;;

(set! the-global-environment (setup-environment))
(mc-eval '(+ 1 2) the-global-environment)

(define (install-primitive! name proc)
  ;; add procedure name
  (set-car! (car the-global-environment)
        (append (caar the-global-environment) (list name)))
  ;; add the procedure itself
  (set-car! the-global-environment
        (append (car the-global-environment) (list (list 'primitive proc))))
  (display "new primitive added: ") (display name) (newline))

;; uten square
;(read-eval-print-loop)

(install-primitive! 'square (lambda (x) (* x x)))

;; med square, og eventuelle andre definisjoner fra meta-REPL-et
(read-eval-print-loop)


;;
;; 3 a-d ligger i evaluator.scm, det er markert med kommentarer
;; hva som tilhører hvilke opppgaver.
;;

