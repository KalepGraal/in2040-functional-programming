;; Oblig 1b - Chrisfku

;; 1 a)

;;  ___      ___
;; |.|.| -> |.|/|
;;  |        |
;; 42       11


;; 1 b)

;;  ___ 
;; |.|/|
;;  |
;; 42

;; 1 c)

;;  ___      ___
;; |.|.| -> |.|/|
;;  |        |
;; 42       11

;; 1 d)

;;  ___      ___
;; |.|.| -> |.|/|
;;  |        |
;; 42        ___      ___
;;          |.|.| -> |.|/|
;;           |        |
;;          11       12

;; 1 e)

;;  ___      ___      ___      ___
;; |.|.| -> |.|.| -> |.|.| -> |.|/|
;;  |        |        |        |
;;  |        1        2        3
;;  ___      ___      ___
;; |.|.| -> |.|.| -> |.|/|
;;  |        |        |
;;  1        2        3


;; 1 f)
(car(cdr '(0 42 #t bar)))
;; 1 g)
(car(cdr(car '((0 42) (#t bar)))))
;; 1 h)
(car(car(cdr '((0) (42 #t) (bar)))))

;; 1 i)

;; kun ved bruk av cons
(cons (cons 0 (cons 42 '())) (cons (cons #t (cons 'bar '())) '()))

;; kun ved bruk av list
(list (list 0 42) (list #t 'bar)) ;; Kan "'" brukes her?

;; 2 a)

(define (length2 items)
  (define (length2-iter acc c-items)
    (if (null? c-items)
        acc
        (length2-iter (+ acc 1) (cdr c-items))))
  (length2-iter 0 items))

;; 2 b)

;; Her har jeg brukt halerekursjon. Valgte dette fordi det var den
;; første løsningen jeg kom på.
(define (rev-list items)
  (define (rev-iter in out)
    (if (null? in)
        out
        (rev-iter (cdr in)
                  (cons (car in) out))))
  (rev-iter items '()))

;; 2 c)

(define (all? pred items)
  (if (null? items)
      #t
      (if (pred (car items))
          (all? pred (cdr items))
          #f)))

(all? odd? '(1 3 5 7 9)) ;; -> #t
(all? odd? '(1 2 3 4 5)) ;; -> #f
(all? odd? '()) ;; -> #t

'(kall med lambda-uttrykk)
(all? (lambda (x) (<= x 10)) '(1 2 3 4 5))
(all? (lambda (x) (<= x 10)) '(1 2 3 4 50))

;; 2 d)

(define (nth index items)
  (define (nth-iter acc it-items)
    (if (= index acc)
        (car it-items)
        (nth-iter (+ acc 1) (cdr it-items))))
  (nth-iter 0 items))

(nth 2 '(47 11 12 13)) ;; -> 12

;; 2 e)

(define (where num items)
  (define (where-iter pos it-items)
    (if (null? it-items)
        #f
        (if (= num (car it-items))
            pos
            (where-iter (+ pos 1) (cdr it-items)))))
  (where-iter 0 items))

(where 3 '(1 2 3 3 4 5 3)) ; -> 2
(where 0 '(1 2 3 3 4 5 3)) ; -> #f

;; 2 f)

(define (map2 proc items1 items2)
  (if (or (null? items1)
          (null? items2))
      '()
      (cons (proc (car items1) (car items2))
            (map2 proc (cdr items1) (cdr items2)))))

(map2 + '(1 2 3 4) '(3 4 5)) ; -> (4 6 8)

;; 2 g)
(map2 (lambda (x y) (/ (+ x y) 2))
      '(1 2 3 4) '(3 4 5)) ; -> (2 3 4)

;; 2 h)

(define (both? pred)
  (lambda (x y)
    (and (pred x)
         (pred y))))

(map2 (both? even?) '(1 2 3) '(3 4 5)) ; -> (#f #t #f)
((both? even?) 2 4) ; -> #t
((both? even?) 2 5) ; -> #f

;; 2 i)
(define (self proc)
  (lambda (x) (proc x x)))

((self +) 5) ; -> 10
((self *) 3) ; -> 9
(self +) ; -> #<procedure>
((self list) "hello") ; ("hello" "hello")

