(load "prekode3a.scm")


;;
;; 1 a+b
;;

;; Is there a good way to avoid this global table, without adding some
;; custom parameters to the memoized procedure?
(define procs (make-table))

(define (mem cmd proc)
  (define (memoize proc)
    (let ((table (make-table)))
      (let ((new-proc
             ;; create new memoized procedure
             (lambda args
               (or (lookup args table)
                   (let ((x (apply proc args)))
                     (insert! args x table)
                     x)))))

        ;; Add reference from the new procedure to the old procedure
        ;; in procs (to support unmemoize)
        (insert! new-proc proc procs)
        new-proc)))
  
  (define (unmemoize proc)
    (or (lookup proc procs)
        (display "could not unmemoize!")))

  (cond ((equal? cmd 'memoize) (memoize proc))
        ((equal? cmd 'unmemoize) (unmemoize proc))
        (else (display "unknown usage"))));)

(set! fib (mem 'memoize fib))
(fib 3)
;computing fib of 3
;computing fib of 2
;computing fib of 1
;computing fib of 0
;→ 2
(fib 3)
;→ 2
(fib 2)
;→ 1
(fib 4)
;computing fib of 4
;→ 3
(set! fib (mem 'unmemoize fib))
(fib 3)
;computing fib of 3
;computing fib of 2
;computing fib of 1
;computing fib of 0
;computing fib of 1
;→ 2

(set! test-proc (mem 'memoize test-proc))
(test-proc)
;computing test-proc of ()
;→ 0
(test-proc)
;→ 0
(test-proc 40 41 42 43 44)
;computing test-proc of (40 41 42 43 44)
;computing test-proc of (41 42 43 44)
;computing test-proc of (42 43 44)
;computing test-proc of (43 44)
;computing test-proc of (44)
;→ 10
(test-proc 40 41 42 43 44)
;→ 10
(test-proc 42 43 44)
;→ 5

;;
;; 1 c
;;
;; Poblemet her er at når vi ikke endrer hva fib er satt til, vil den originale
;; fib bli kalt rekursivt, ettersom denne ikke lagrer svarene underveis, vil
;; kun det argumentet vi kaller mem-fib med bli lagret til senere bruk.
;;

;;
;; 1 d
;;

(define (make-arg-handler . args)
  (let ((table (make-table)))
    (define (add-keys! k)
      (if (pair? k)
          (begin (insert! (car k) (cadr k) table)
                 (add-keys! (cddr k)))))
    (add-keys! args)
    
    (lambda (cmd args)
      (cond ((equal? cmd 'parse)
             (add-keys! args))
            ((equal? cmd 'get)
             (or (lookup args table)
                 "uninitialized-arg"))))))

(define (greet . args)
  (let ((arg-handler (make-arg-handler 'time "day"
                                       'title "friend")))
    (arg-handler 'parse args)
    (display "good ") (display (arg-handler 'get 'time))
    (display " ") (display (arg-handler 'get 'title))
    (newline)))


(greet)
;good day friend
(greet 'time "evening")
;good evening friend
(greet 'title "sir" 'time "morning")
;good morning sir
(greet 'time "afternoon" 'title "dear")
;good afternoon dear


;;
;; 2 a
;;

(define (list-to-stream list)
  (if (null? list)
      the-empty-stream
      (cons-stream (car list) (list-to-stream (cdr list)))))

(define (stream-to-list stream . n)
  (define (stl-rec stream i)
    (cond ((= i 0) '())
          ((stream-null? stream) '())
          (else (cons (stream-car stream)
                      (stl-rec (stream-cdr stream) (- i 1))))))
  (if (null? n)
      (stl-rec stream -1)
      (stl-rec stream (car n))))


(list-to-stream '(1 2 3 4 5))
;→ (1 . #<promise>)
(stream-to-list (stream-interval 10 20))
;→ (10 11 12 13 14 15 16 17 18 19 20)
(show-stream nats 15)
; 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 ...
(stream-to-list nats 10)
;→ (1 2 3 4 5 6 7 8 9 10)


;;
;; 2 b
;;

(define (stream-map proc . argstreams)
  (if (memq '() argstreams)
      the-empty-stream
      (cons-stream
       (apply proc (map stream-car argstreams))
       (apply stream-map
              (cons proc (map stream-cdr argstreams))))))


;;
;; 2 c
;;
;; Problemet oppstår når vi modifiserer memq, da memq må evaluere
;; hele strømmen, noe som er problematisk dersom strømmen er uendelig.
;;


;;
;; 2 d
;;

(define (remove-duplicates stream)
  (if (stream-null? stream)
      the-empty-stream
      (cons-stream (stream-car stream)
                   (remove-duplicates
                    (stream-filter
                     (lambda (x) (not (eq? x (stream-car stream))))
                     (stream-cdr stream))))))

;;
;; 2 e
;;

;; på REPL'et evalueres dette slik:

;; ? (define x
;;     (stream-map show
;;                 (stream-interval 0 10)))
;; > 0
;; ? (stream-ref x 5)
;; > 1
;; > 2
;; > 3
;; > 4
;; > 5
;; > 5
;; ? (stream-ref x 7)
;; > 6
;; > 7
;; > 7

;; Når vi definerer x, vil show først kalles i stream-map for å finne første
;; element i strømmen. (derved printes 0).
;;
;; når vi kaller stream-ref, må stream-map bygge de neste elementene
;; i strømmen som den returnerer, og dette gjør den ved å kalle show
;; på de 5 neste elementene, slik at stream-ref kan finne element på indeks 5.
;; Dette fører til at 1, 2, 3, 4 og 5 printes.
;;
;; Deretter kalles stream-ref igjen, og siden vi i strømmen x allerede har de
;; 6 første elementene, må stream-map kun kalle show på elementene på indeks
;; 6 og 7, her vil altså 6 og 7 printes.


;;
;; 2 f
;;

(define (mul-streams . streams)
  (apply stream-map * streams))

;;
;; 2 g
;;

(define factorials (cons-stream 1 (mul-streams nats factorials)))
(stream-ref factorials 5); → 120