(load "huffman.scm")

;;
;; 1 a
;;
(newline) (display "1 a)") (newline)

(define (p-cons x y)
  (lambda (proc) (proc x y)))

(define (p-car cell)
  (cell (lambda (x y) x)))

(define (p-cdr cell)
  (cell (lambda (x y) y)))

(p-cons "foo" "bar") ; → #<proc>
(p-car (p-cons "foo" "bar")) ; → "foo"
(p-cdr (p-cons "foo" "bar")) ; → "bar"
(p-car (p-cdr (p-cons "zoo" (p-cons "foo" "bar")))) ; → "foo"

;;
;; 1 b
;;
(newline) (display "1 b)") (newline)

(define foo 42)

;; Evaluerer til different
((lambda (foo x)
   (if (= x foo)
       'same
       'different))
 5 foo)

;; Evaluerer til (towel (42 towel))
((lambda (bar baz)
   ((lambda (bar foo)
      (list foo bar))
    (list bar baz) baz))
 foo 'towel)

;;
;; 1 c
;;
(newline) (display "1 c)") (newline)

(define foo (list 21 + 21))
(define baz (list 21 list 21))
(define bar (list 84 / 2))

(define (infix-eval exp)
  ((cadr exp) (car exp) (caddr exp)))

(infix-eval foo) ;; → 42
(infix-eval baz) ;; → (21 21)
(infix-eval bar) ;; → 42

;;
;; 1 d
;;
;; (define bah '(84 / 2))
;; (infix-eval bah)
;;
;; Dette fører til en feil, da / blir tolket som symbolet /, og ikke
;; som prosedyren divisjon. Dette er på grunn av bruken av quote (').

;;
;; 2 a
;;
;; Hver gang vi har dekodet et symbol, starter vi på nytt fra roten til treet,
;; da trenger vi tree variabelen. Hjelpeprosedyren bruker vi til å kunne
;; traversere treet (holde styr på hvilket subtree vi for øyeblikket sjekker).
;; Tror vi kunne lagt til tree som en parameter til decode-1 og droppet den ytre
;; definisjonen, men dette ville ført til en unødvendig ekstra parameter når man
;; kaller på decode utenifra. Kort sagt brukes hjelpeprosedyren for å forenkle
;; antall parametrene som trengs i decode.

;;
;; 2 b
;;

(define (decode bits tree)
  (define (decode-1 bits current-branch decoded)
    (if (null? bits)
        (reverse decoded)
        (let ((next-branch (choose-branch (car bits)
                                          current-branch)))
          (if (leaf? next-branch)
              (decode-1 (cdr bits) tree
                        (cons (symbol-leaf next-branch) decoded))
              (decode-1 (cdr bits) next-branch decoded)))))
  (decode-1 bits tree '()))

;;
;; 2 c
;;
(newline) (display "2 c)") (newline)

(decode sample-code sample-tree)
;; gir oss: (ninjas fight ninjas by night)

;;
;; 2 d
;;
(newline) (display "2 d)") (newline)

(define (encode msg tree)
  (if (null? msg)
      '()
      ;; encode each character by itself
      (append (encode-char (car msg) tree)
              (encode (cdr msg) tree))))

(define (encode-char char tree)
  (define (encode-dfs subtree bits)
    (if (leaf? subtree)
        ;; in leaf node, check if this is correct char, if not #f
        (if (eq? char (symbol-leaf subtree))
            bits
            #f)
        
        ;; not in leaf node
        (or (encode-dfs (left-branch subtree) (cons 0 bits))
            (encode-dfs (right-branch subtree) (cons 1 bits)))))
  ;; reverse results, as bit order is backwards
  (reverse (encode-dfs tree '())))

(decode (encode '(ninjas fight ninjas) sample-tree) sample-tree)
;; → (ninjas fight ninjas)

;;
;; 2 e
;;
(newline) (display "2 e)") (newline)

(define (grow-huffman-tree freqs)
  (define (grow-huffman-tree-1 freqs)
    (if (null? (cdr freqs))
        ;; only one element, tree creation done
        freqs

        ;; more than one element, create new-tree from first two nodes
        (let ((new-tree (make-code-tree (car freqs)
                                        (cadr freqs))))
          (if (null? (cddr freqs))
              ;; no more nodes to merge, return finished tree
              new-tree
              
              ;; more nodes to merge
              (grow-huffman-tree-1 (adjoin-set new-tree
                                               (cddr freqs)))))))
  (grow-huffman-tree-1 (make-leaf-set freqs)))

(define freqs '((a 2) (b 5) (c 1) (d 3) (e 1) (f 3)))
(define codebook (grow-huffman-tree freqs))
(decode (encode '(a b c) codebook) codebook); → (a b c)

;;
;; 2 f
;;
(newline) (display "2 f)") (newline)

(define words '((samurais 57) (ninjas 20) (fight 45) (night 12) (hide 3) (in 2)
                              (ambush 2) (defeat 1) (the 5) (sword 4) (by 12)
                              (assassin 1) (river 2)(forest 1) (wait 1)
                              (posion 1)))

(define message '(ninjas fight
                         ninjas fight ninjas
                         ninjas fight samurais
                         samurais fight
                         samurais fight ninjas
                         ninjas fight by night))

(define code-tree (grow-huffman-tree words))
(define encoded-msg (encode message code-tree))
encoded-msg

;; teller hvor mange bits som trengs for å kode meldingen:
(define (count-args x)
  (if (null? x)
      0
      (+ 1 (count-args (cdr x)))))
(count-args encoded-msg)
;; - dette gir 43, altså tar det 43 bits for å kode meldingen

;; litt usikker hva som blir spurt om her, men antar det er bits per kodeord?
;; isåfall:
(/ (count-args encoded-msg) (count-args message))
;; - dette gir oss et snitt på 43/17 bits per melding

;; hvis vi skulle hatt fixed length code, måtte hver melding representeres av
(log (count-args words) 2) ; -> 4
;; altså trenger vi 4 bits per melding, og hele meldingen ville da blitt
;; måtte blitt representert av 4 * 17 = 68 bits.
;; ved å bruke huffmankoding sparer vi i dette tilfellet 25 bits.

;;
;; 2 g
;;
(newline) (display "2 g)") (newline)

(define (huffman-leaves tree)
  (if (leaf? tree)
      (list (list (symbol-leaf tree) (weight-leaf tree)))
      (append (huffman-leaves (left-branch tree))
              (huffman-leaves (right-branch tree)))))
(huffman-leaves sample-tree); → ((ninjas 8) (fight 5) (night 1) (by 1))


;;
;; 2 h
;;
(newline) (display "2 h)") (newline)

(define (expected-codeword-length tree)
  (define (ecl-iter subtree numbits)
    (if (leaf? subtree)
        ;; is leaf, calculate p(s_i)*|c_i|
        (* (/ (weight subtree)
              (weight tree))
           numbits)
        ;; not leaf, calculate sum of subtrees
        (+ (ecl-iter (left-branch subtree) (+ 1 numbits))
           (ecl-iter (right-branch subtree) (+ 1 numbits)))))
  (ecl-iter tree 0))

(expected-codeword-length sample-tree) ;  → 1+3/5
